use std::collections::VecDeque;
use std::time::Instant;

use anyhow::Result;
use clap::Parser;
use cli::Args;
use crossterm::event::{read, Event, KeyCode, KeyEvent, KeyEventKind};
use crossterm::terminal;

mod cli;

fn main() -> Result<()> {
    let args = Args::parse();

    let sample_size = args.sample_size as usize;
    let conversion_factor = 60.0 * 1000.0 * (sample_size - 1) as f64;

    println!(
        "Waiting for {} taps to compute the tempo, press q to quit",
        sample_size
    );

    let mut instants: VecDeque<Instant> = VecDeque::with_capacity(sample_size);

    terminal::enable_raw_mode()?;

    loop {
        if let Event::Key(KeyEvent {
            code,
            kind: KeyEventKind::Press | KeyEventKind::Repeat,
            ..
        }) = read()?
        {
            if code == KeyCode::Char('q') {
                break;
            }
            instants.push_back(Instant::now());

            if instants.len() > sample_size {
                instants.pop_front();
            }

            if instants.len() == sample_size {
                let first = instants.front().unwrap();
                let last = instants.back().unwrap();
                let elapsed = last.duration_since(*first);
                let bpm = conversion_factor / elapsed.as_millis() as f64;
                println!("\r{:.2} bpm", bpm);
            }
        }
    }

    terminal::disable_raw_mode()?;

    Ok(())
}
