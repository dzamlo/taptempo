use clap::Parser;

/// A clone of tap tempo in rust
#[derive(Parser, Debug)]
#[command(author, version, about)]
pub struct Args {
    /// number of taps used to compute the tempo
    #[arg(short, long, default_value = "5", value_parser = clap::value_parser!(u32).range(2..))]
    pub sample_size: u32,
}
